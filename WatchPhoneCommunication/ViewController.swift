//
//  ViewController.swift
//  WatchPhoneCommunication
//
//  Created by swathi  on 2019-06-28.
//  Copyright © 2019 swathi . All rights reserved.
//

import UIKit
import WatchConnectivity


class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
    }
    
    
    @IBOutlet weak var TextBox: UITextField!
    
    @IBOutlet weak var DisplayMsgFromWatch: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //checking if sessioinis supported
        if (WCSession.isSupported()) {
            print("PHONE: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("PHONE: Phone does not support WatchConnectivity")
        }
    }
    // receive msg from watch
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
       
        var sendAcknowledgement = Dictionary<String, String>()
        self.DisplayMsgFromWatch.text = message["watchmsg"] as? String
        
        if(message["watchmsg"] != nil){
            sendAcknowledgement["ack"] = "PHONE RECEIVED MSG"
            
        }
        else {
            sendAcknowledgement["ack"] = "PHONE NOT RECEIVED MSG"
        }
        replyHandler(sendAcknowledgement as [String : AnyObject])
    }
    
    
    
    // send msg to watch
    @IBAction func sendmsgtowatch(_ sender: Any) {
        
        print("Button Clicked")
        
        if (WCSession.default.isReachable) {
            print("Reachable")
            let message:[String : Any] = ["msg" : TextBox.text!]
            WCSession.default.sendMessage(message
                ,replyHandler: {replyMessage in
            }
                
                , errorHandler: {error in
                    print( error.localizedDescription)})
            
        }
    }
    
  
    

}


