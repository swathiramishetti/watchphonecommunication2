//
//  InterfaceController.swift
//  WatchPhoneCommunication WatchKit Extension
//
//  Created by swathi  on 2019-06-28.
//  Copyright © 2019 swathi . All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity



class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
    }
    

    @IBOutlet weak var MsgDisplay: WKInterfaceLabel!
    
    @IBOutlet weak var acknowledgement: WKInterfaceLabel!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        print("--- WATCH APP IS LOADED!")
        //activating session
        if (WCSession.isSupported()) {
            print("Watch: Phone supports WatchConnectivity!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        else {
            print("Watch: Phone does not support WatchConnectivity")
        }

    }
    // To display msg from phone
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        print("Watch Got the message")
        print(message["msg"] as! String)
        self.MsgDisplay.setText(message["msg"] as? String)
        
            }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
// To send msg to  phone
    @IBAction func sendMSG2phone() {
        
        
        if (WCSession.default.isReachable) {
            let watchmessage = ["watchmsg": "Hello from watch"]
            WCSession.default.sendMessage(watchmessage
                ,replyHandler: {replyMessage in
                    self.acknowledgement.setText(replyMessage["ack"] as? String)
            }
                
                , errorHandler: {error in
                    print( error.localizedDescription)})
            
        }
        
    
    }
    

}
